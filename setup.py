from setuptools import setup
import versioneer

setup(name='loosolab_kube',

      description='Kube: A wrapper Package for the main Kube objects and Functions',
      author='Philipp Goymann',
      author_email='philipp.goymann@mpi-bn.mpg.de',
      license='MIT',
      packages=['loosolab_kube', 'loosolab_kube.Kube_objects', 'loosolab_kube.Utils', 'loosolab_kube.High_level_functions'],
      install_requires=[
      'pyyaml', 
      'requests',
      'kubernetes==24.2.0'
      ],
      dependency_links=['https://gitlab.gwdg.de/loosolab/software/loosolab-s3/-/archive/master/loosolab-s3-master.tar'],
      classifiers = [
        'Programming Language :: Python :: 3'
      ],
      zip_safe=False,
      include_package_data=True ,
      
      version=versioneer.get_version(),
      cmdclass=versioneer.get_cmdclass())


#install dependencies from loosolab-s3

import subprocess
import sys
subprocess.check_call([sys.executable, "-m", "pip", "install", 'git+https://gitlab.gwdg.de/loosolab/software/loosolab-s3.git'])

