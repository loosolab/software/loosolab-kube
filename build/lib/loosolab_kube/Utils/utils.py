import sys
import re
#self import
from loosolab_kube.Utils.Logger import KubernetesLogger

class  Utils():
    def __init__(self, tool_name="Kube", level=3):
        self.logger = KubernetesLogger(tool_name=tool_name, level=level)
        self.logger.begin()

    #--------------------------------------------------------------------------------------------------------#

    def check_if_list_obj(self, obj, message='Object'):
        if isinstance(obj, list) or obj == None:
            self.logger.debug(message + ' is of type list!')
            return obj
        elif isinstance(obj, str):
            self.logger.debug(message + ' converted to list!')
            return [obj]
        else:
            self.logger.error(message + ' must be of type list!')
            raise ValueError

    #--------------------------------------------------------------------------------------------------------#

    def check_if_string_is_dns_1123_valid(self, string):
        if re.match(r"[a-z0-9]([-a-z0-9][a-z0-9])?(\\.[a-z0-9]([-a-z0-9][a-z0-9])?)", string):
            self.logger.debug(string + ' Is dns 1123 valid type!')
        else:
            self.logger.error("String isn't dns 1123 valid: " + string +' Must be of lower case and only contian \'-\' or \'.\' ! ')
            raise ValueError

    #--------------------------------------------------------------------------------------------------------#

    def check_if_file_exists(self, files):
        if len(files) == 0:
            logging.error( "Their are no Files given!")
            sys.exit(1)
        for i in files:
            if os.path.exists(i):
                logging.info('File ' + i + ' exist!')
                pass
            else:
                logging.error('File ' + i + ' didn\'t exist!')
                raise FileNotFoundError     

            
            
