#!/usr/bin/env python3
import kubernetes
from kubernetes import client, config
from kubernetes.client.rest import ApiException
from kubernetes.stream import stream

import tarfile
from tempfile import TemporaryFile

import sys

# self import

from .Kube import Kube
from loosolab_kube.Utils.utils import Utils


class Pod(Kube, Utils):
    def __init__(self, namespace, level=3, config_file= False):
        """
            Creates empty Pod object and checks namespace.
        """
        super().__init__(namespace,  tool_name="Pod", level=level, config_file= config_file)
        self.body_of_Pod = client.V1Pod('v1', 'Pod') 
        self.body_of_Pod.metadata =  client.V1ObjectMeta()

    #--------------------------------------------------------------------------------------------------------#

    def build_Pod_obj(self, name_of_Pod, container, init_container=None):
        """
            creates a Pod object.
            required: 
                name_of_Pod
                container = container description can be created with Container function.
            optional:
                init_Container = Creates an init_Container for the Pod must be given as list.
        """
        self.check_if_list_obj(container, message='Container')#container must be given as list obj when missing sys exit
        self.check_if_list_obj(init_container, message='InitContainer')
        self.body_of_Pod.metadata.name = name_of_Pod
        if init_container == None:
            self.body_of_Pod.spec = client.V1PodSpec(containers = container)
        else:
            self.body_of_Pod.spec = client.V1PodSpec(containers = container, init_containers = init_container)
        
        return self.body_of_Pod

    #--------------------------------------------------------------------------------------------------------#

    def check_if_Pod_ex(self, name_of_Pod):
        #Checks if a Pod exists on the Cluster.
        try:
            api_response = self.CoreV1Api.list_namespaced_pod(self.namespace, field_selector='metadata.name=' + name_of_Pod)
        except ApiException as e:
            self.logger.error('Cloud not list Pod\'s' + str(e))
            raise Exception('Could not list Pods' + str(e))
        if len(api_response.items) != 0:
            self.logger.info('Pod {} exists!'.format(name_of_Pod))
            return True
        else:
            self.logger.warning('Pod {} didn\'t exists!'.format(name_of_Pod))
            return False

    #--------------------------------------------------------------------------------------------------------#

    def create_Pod(self, body_of_Pod ):
        #Sends a Give Pod object to the Cluster if it isn't Present.
        if self.check_if_Pod_ex(body_of_Pod.metadata.name):
            self.logger.error('Pod {} already exists!'.format(body_of_Pod.metadata.name))

        try:#create new secret
            self.CoreV1Api.create_namespaced_pod(self.namespace, body_of_Pod)
            self.logger.info('Created Pod: ' + body_of_Pod.metadata.name)
        except ApiException as e:
            self.logger.error('Could not create Pod:' + str(e))
            raise Exception('Could not create Pod:' + str(e))


    #--------------------------------------------------------------------------------------------------------#

    def delete_Pod(self, name_of_Pod):
        #Deletes a Pod on the Cluster
        if not self.check_if_Pod_ex(name_of_Pod):
            self.logger.error('Could not delete {}, Pod doesn\'t exists!'.format(name_of_Pod))
            return
        try:#create new secret
            self.CoreV1Api.delete_namespaced_pod(name_of_Pod, self.namespace)
            self.logger.info('Deleted Pod: '+ name_of_Pod)
        except ApiException as e:
            self.logger.error('Could not delete Pod: ' + str(e))
            raise Exception('Could not delete Pod: ' + str(e))

    #--------------------------------------------------------------------------------------------------------#
    
    def list_all_Pods(self, full_information = False):
        try:
            pods_of_namespace = self.CoreV1Api.list_namespaced_pod(self.namespace, pretty=True)
            if full_information:
                self.logger.info('Returned description of all Pods.')
                return pods_of_namespace
            else:
                self.logger.info('Returned list of all Pods names.')
                return [i.metadata.name for i in pods_of_namespace.items]
        except ApiException as e:
            self.logger.error('Listing all Pods failed: ' + str(e))
            raise Exception('Listing all Pods failed: ' + str(e))

    #--------------------------------------------------------------------------------------------------------#
    
    def send_command_to_pod(self, name_of_Pod, exec_command,container_name='main-container'): 
        #exec_command = ['/bin/sh','-c','ls']
        if not self.check_if_Pod_ex(name_of_Pod):
            self.logger.error('Could not execute command in {}, Pod does not exist!'.format(name_of_Pod))
            return        
        try:
            resp = stream(self.CoreV1Api.connect_get_namespaced_pod_exec, name_of_Pod, self.namespace, container=container_name,
                        command=exec_command,
                        stderr=True, stdin=False,
                        stdout=True, tty=False)
            self.logger.info('Run command!')
            self.logger.debug('Execute command ' + str(exec_command) + ' in Pod: ' + name_of_Pod) 
            # recreate api:
            #super().__init__(namespace,  tool_name="Pod", level=level)
            self.body_of_Pod = client.V1Pod('v1', 'Pod') 
            self.body_of_Pod.metadata =  client.V1ObjectMeta()
            
            return resp
        except Exception as e:
            self.logger.error('Could not excecute command: ' + str(exec_command) + ' in Pod: ' + name_of_Pod)
            raise Exception('Could not excecute command: ' + str(exec_command) + ' in Pod: ' + name_of_Pod + str(e))
        '''
        after exec command rebuild the api?
        https://github.com/kubernetes-client/python#why-execattach-calls-doesnt-work
        '''

    #--------------------------------------------------------------------------------------------------------#
    
    def copy_file_from_pod(self, name_of_Pod, src_path, dest_path, container_name='main-container'):
        """
        https://github.com/prafull01/Kubernetes-Utilities/blob/master/kubectl_cp_as_python_client.py
        """

        exec_command = ['tar', 'cf', '-', src_path]
        try:
            with TemporaryFile() as tar_buffer:
                resp = stream(self.CoreV1Api.connect_get_namespaced_pod_exec, name_of_Pod, self.namespace,  container=container_name,
                                command=exec_command,
                                stderr=True, stdin=True,
                                stdout=True, tty=False,
                                _preload_content=False) # stdin=T & preload=F return an open stream instead of string
                self.logger.info('Start file download!')

                while resp.is_open():
                    resp.update(timeout=1)
                    if resp.peek_stdout():
                        out = resp.read_stdout()
                        # print("STDOUT: %s" % len(out))
                        tar_buffer.write(out.encode('utf-8'))
                    if resp.peek_stderr():
                        self.logger.error("Problem downloading files: %s" % resp.read_stderr())
                resp.close()

                tar_buffer.flush()
                tar_buffer.seek(0)

                with tarfile.open(fileobj=tar_buffer, mode='r:') as tar:
                    for member in tar.getmembers():
                        if member.isdir():
                            continue
                        fname = member.name.rsplit('/', 1)[1]
                        tar.makefile(member, dest_path + '/' + fname)

                self.logger.info('File download finished! See: ' + dest_path)
        except Exception as e:
            self.logger.error('Could not download ' + str(src_path) + ' from Pod: ' + name_of_Pod)
            raise Exception('Could not download ' + str(src_path) + ' from Pod: ' + name_of_Pod + str(e))
