import sys
import os
from datetime import datetime
import logging
import logging.handlers
import multiprocessing as mp
import time

class  KubernetesLogger(logging.Logger):
	""" KubeLogger is an instance of a logging.Logger with special functions for formatting and creating automatic logging.
        This class was copied from TOBIAS logger.py"""

	logger_levels = {
						0: 0,
						1: logging.WARNING,							#also includes errors
						2: logging.INFO, 							#info 
						3: int((logging.INFO+logging.DEBUG)/2),		#statistics
						4: logging.DEBUG,							#debugging info
						5: logging.DEBUG - 5						#spam-level debug
					}

	def __init__(self, tool_name="", level=3, queue=None):

		self.tool_name = tool_name		#name of tool within Kubernetes_TOOL
		logging.Logger.__init__(self, self.tool_name)
		if level == 0:
			self.disabled = True

		####### Setup custom levels #######
		#Create custom level for comments (Same level as errors/warnings)
		comment_level = KubernetesLogger.logger_levels[1] + 1
		logging.addLevelName(comment_level, "comment") #log_levels[lvl])
		setattr(self, 'comment', lambda *args: self.log(comment_level, *args))

		#Create custom level for stats (between info and debug)
		stats_level = KubernetesLogger.logger_levels[3]
		logging.addLevelName(stats_level, "STATS") #log_levels[lvl])
		setattr(self, 'stats', lambda *args: self.log(stats_level, *args))
		
		#Create custom level for spamming debug messages
		spam_level = KubernetesLogger.logger_levels[5]
		logging.addLevelName(spam_level, "SPAM") #log_levels[lvl])
		setattr(self, 'spam', lambda *args: self.log(spam_level, *args))

		#Set level
		self.level = KubernetesLogger.logger_levels[level]
		
		######### Setup formatter ########

		#Setup formatting
		self.formatter = KubernetesFormatter()
		self.setLevel(self.level)

		if queue == None:
			#Stdout stream
			con = logging.StreamHandler(sys.stdout)		#console output
			con.setLevel(self.level)
			con.setFormatter(self.formatter)
			self.addHandler(con)
		else:
			h = logging.handlers.QueueHandler(queue)  	# Just the one handler needed
			self.handlers = []
			self.addHandler(h)

		#Lastly, initialize time
		self.begin_time = datetime.now()
		self.end_time = None
		self.total_time = None


	def begin(self):
		""" Begin logging by writing comments about the current run """
		#Print info on run
		self.cmd =  " ".join(sys.argv[1:])
		self.comment("# Kubeloosolab {0} instance (run started {1})".format(self.tool_name, self.begin_time))
		self.comment("# Working directory: {0}".format(os.getcwd()))

	def stop(self):
		""" Stop without printing status """
		self.end_time = datetime.now()
		self.total_time = self.end_time - self.begin_time
		
	def end(self):
		""" End logging - write out how long it took """

		self.end_time = datetime.now()
		self.total_time = self.end_time - self.begin_time
		self.comment("")	#creates empty line; only for pretty output
		self.info("Finished {0} run (total time elapsed: {1})".format(self.tool_name, self.total_time))


class KubernetesFormatter(logging.Formatter):
	""" Formatter class used in KubernetesLogger """
	default_fmt = logging.Formatter("%(asctime)s (%(process)d) [%(levelname)s]\t%(message)s", "%Y-%m-%d %H:%M:%S")
	comment_fmt = logging.Formatter("%(message)s")

	def format(self, record):
		if record.levelname == "comment":
			return self.comment_fmt.format(record)
		elif record.levelno != 0:
			return self.default_fmt.format(record)	
		else:
			return