#!/usr/bin/env python3
import kubernetes
from kubernetes import client, config
from kubernetes.client.rest import ApiException
import base64   
import sys
import json
# self import

from .Kube import Kube

class Secret(Kube):


    def __init__(self, namespace, level=3, config_file= False):
        super().__init__(namespace,  tool_name="Secret", level=level, config_file= config_file)
        #Checks the namespace and creates an empty S3-Secret
        self.body_of_secret = kubernetes.client.V1Secret(api_version = 'v1', kind = 'Secret')
        self.body_of_secret.metadata = kubernetes.client.V1ObjectMeta(namespace = self.namespace)

    #--------------------------------------------------------------------------------------------------------#

    def build_Secret(self, data_dict, name_of_secret):
        """
            Creates a Secret which  contains the key:values from a dict.
            Key values get automated convert to base64 format.
        """
        try:
            self.body_of_secret.metadata.name = name_of_secret
            
            self.body_of_secret.data = {key:base64.b64encode(value.encode('ascii')).decode() for key, value in data_dict.items()}
            return self.body_of_secret
        except Exception as e:
            raise Exception('Could not build Secret ' + str(e))

    #--------------------------------------------------------------------------------------------------------#

    def build_s3_Secret(self,  endpoint, s3key, s3secret, name_of_secret):
        """
            Creates a S3-Secret the values are accessible by the containers on the 
            cluster over the keys s3endpoint, s3key and s3secret.
        """
        self.body_of_secret.metadata.name = name_of_secret
        self.body_of_secret.data = {'s3endpoint': base64.b64encode(endpoint.encode('ascii')).decode(), 
            's3key': base64.b64encode(s3key.encode('ascii')).decode() , 
                's3secret': base64.b64encode(s3secret.encode('ascii')).decode()}
        return self.body_of_secret

    #--------------------------------------------------------------------------------------------------------#

    def create_Secret(self, body_of_secret):
        #Creates a secret on the cluster.
        if self.check_if_Secret_ex(body_of_secret.metadata.name):
            self.logger.error('Secret {} already exists!'.format((body_of_secret.metadata.name)))
        #create new secret
        try:
            self.CoreV1Api.create_namespaced_secret(self.namespace, body_of_secret)
            self.logger.info('Created secret: '+ body_of_secret.metadata.name)
        except ApiException as e:
            self.logger.error('Created secrect ERROR: ' + str(e))
            raise Exception('Created secrect ERROR: ' + str(e))

    #--------------------------------------------------------------------------------------------------------#

    def check_if_Secret_ex(self, name_of_secret):
        #Checks if a secret exists on the cluster 
        try:
            api_response = self.CoreV1Api.list_namespaced_secret(self.namespace, field_selector='metadata.name=' + name_of_secret)
        except ApiException as e :
            self.logger.error('Cloud not list Secrests' + str(e))
            raise Exception('Could not list Secrets' + str(e))
        if len(api_response.items) != 0:
            self.logger.info('Secret {} exists!'.format(name_of_secret))
            return True
        else:
            self.logger.warning('Secret {} does not exists!'.format(name_of_secret))
            return False

    #--------------------------------------------------------------------------------------------------------#

    def delete_Secret(self, name_of_secret):
        #delete a secret on the Cluster
        if not self.check_if_Secret_ex(name_of_secret):
            self.logger.error('Could not delete Secret {} dosen\'t exists!'.format(name_of_secret))
        try:#create new secret
            self.CoreV1Api.delete_namespaced_secret(name_of_secret, self.namespace)
            self.logger.info('Deleted Secret: '+ name_of_secret)
        except ApiException as e:
            self.logger.error('Could not delete Secrect: ' + str(e))
            raise Exception('Could not delete Secrect: ' + str(e))

    #--------------------------------------------------------------------------------------------------------#

    def get_Secret_data(self, name_of_secret):
        #returns the data of a secret on the cluster as a dict.
        if not self.check_if_Secret_ex(name_of_secret):
            self.logger.error('Secret {} dosen\'t exists!'.format(name_of_secret))
        try:
            api_response = self.CoreV1Api.list_namespaced_secret(self.namespace, field_selector='metadata.name=' + name_of_secret)
            return {key:base64.b64decode(val) for key, val in api_response.items[0].data.items()}
        except ApiException as e :
            self.logger.error('Could not list Secrests' + str(e))
            raise Exception('Could not list Secrests' + str(e))

    #--------------------------------------------------------------------------------------------------------#
    
    def list_all_Secrets(self, full_information = False):
        try:
            secrets_of_namespace = self.CoreV1Api.list_namespaced_secret(self.namespace, pretty=True)
            if full_information:
                self.logger.info('Returned description of all Secrets.')
                return secrets_of_namespace
            else:
                self.logger.info('Returned list of all Secrets names.')
                return [i.metadata.name for i in secrets_of_namespace.items]
        except ApiException as e:
            self.logger.error('Listing all Secrets failed: ' + str(e))
            raise Exception('Listing all Secrets failed: ' + str(e))
