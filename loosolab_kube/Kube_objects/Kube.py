#!/usr/bin/env python3

import kubernetes
from kubernetes import client, config
from kubernetes.client.rest import ApiException
#import logging
import sys

#self import
from loosolab_kube.Utils.Logger import KubernetesLogger
from loosolab_kube.Utils.utils import Utils

"""
    This class containing functions to create a high level api for Kubernetes
    This is the lowest class of the packge it contain the function to create 
    the kubernetes api instaces for the other classes. 
"""
class Kube(Utils):

    def __init__(self, namespace,config_file= False, tool_name="Kube", level=3):
        self.logger = KubernetesLogger(tool_name=tool_name, level=level)
        self.logger.begin()
        self.CoreV1Api, self.BatchV1Api, self.AppsV1Api, self.AutoscalingV1Api , self.NetworkingV1Api , self.V1IngressClass = self.create_client_instance(config_file= config_file)
        self.namespace = self.check_namespace(namespace)
    
    #------------------------------------------------ Api and resources functions -------------------------------------------#  
    
    def create_client_instance(self,config_file= False):
        """
            Create the api instance which are needed to create the Kube objects.
        """
        try:
            if config_file:
                config.load_kube_config(config_file)
            else:
                config.load_kube_config()
            configuration = client.Configuration()
            CoreV1Api = client.CoreV1Api()
            BatchV1Api = client.BatchV1Api(kubernetes.client.ApiClient(configuration))# needed for Jobs
            AppsV1Api =  client.AppsV1Api()#client.AppsV1Api(kubernetes.client.ApiClient(configuration))# needed for Deployments
            #V1beta1Api = client.ExtensionsV1beta1Api(kubernetes.client.ApiClient(configuration))#needed to work with ingresses, Deployment
            AutoscalingV1Api = client.AutoscalingV1Api(kubernetes.client.ApiClient(configuration))
            V1IngressClass = client.V1IngressClass() 
            
            NetworkingV1Api = client.NetworkingV1Api()
            self.logger.info('Create API instances')
        except ApiException as e:
            self.logger.error("Exception when creating credentials: %s\n" % e)
            raise Exception("Exception when creating credentials: %s\n" % e)
        return CoreV1Api, BatchV1Api,  AppsV1Api, AutoscalingV1Api, NetworkingV1Api, V1IngressClass

    #------------------------------------------------ Build functions -------------------------------------------------------#

    def volumes(self, name, name_of_PVC=None , name_of_secret = None):
        """
            Creates an emptydir volume of name_of_Pvc isn't set if a PVC volume is set it's creates a
            PVC description.

            name: Name of Volume 

            name_of_PVC: name des PVCs

            return:

            If PVC:
                PVC instaces which can be added to Pod, Job or Deployment.
            else:
                empty dir instance can be added to Pod, Job or Deployment.
        """

        if name_of_PVC == None and name_of_secret == None:
            volume = client.V1Volume(name=name,empty_dir=client.V1EmptyDirVolumeSource())
        elif name_of_secret != None:
            volume = client.V1Volume(name=name, secret=client.V1SecretVolumeSource(secret_name=name_of_secret) )
        else:
            volume = client.V1Volume(name=name,
                persistent_volume_claim=client.V1PersistentVolumeClaimVolumeSource(claim_name=name_of_PVC))
        return volume
    #------------------------------------------------------------------------------------------------------------------------#

    def volume_mounts(self, name, mount_path):
        """
            Create Mountpath description for Container to mount an PVC or emptydir.
        """
        volume_mounts = client.V1VolumeMount(mount_path=mount_path,name=name)
        return volume_mounts

    #------------------------------------------------------------------------------------------------------------------------#

    def env_variables(self, name_of_value, key_of_secret, name_of_secret):
        """
            Mounts Secretes which are availabe on the cluster. As envs in a Container description.
        """
        env = client.V1EnvVar(name=name_of_value, value_from=client.V1EnvVarSource(secret_key_ref=client.V1SecretKeySelector(name=name_of_secret, key=key_of_secret)))
        return env

    #------------------------------------------------------------------------------------------------------------------------#

    def container(self, name_for_container, container_image, comand_for_container=None, comand=None,
     resources={"requests": {'memory': "128Mi", 'cpu': "100m"},"limits": {'memory': "128Mi", 'cpu': "100m"}}, container_port = None,
        volume_mounts=[], env_list=[]):
        """
            Creates a Container instance

            required:
            name_for_Container
            container_image
            optional:
                comand_for_container = Comandline descriptions
                comand =  which comand should use to interpret the container comand for example use 'bash/sh'
                resources = resources of container in a dict with limits and requests
                container_port = default 80 need for expose of traffic
                volume_mounts = volumes which should be mounted iin the container
                env_list = List of env variables for the container 
            return:
                Container description
        """
        self.check_if_list_obj(env_list, message='Env variable')
        self.check_if_list_obj(volume_mounts, message='Volume mounts')
        
        if container_port != None:
            container = client.V1Container(command=comand, args=comand_for_container , image = container_image , name = name_for_container, resources = resources,
                            ports=[client.V1ContainerPort(container_port=container_port)],volume_mounts = volume_mounts,env=env_list, security_context=client.V1SecurityContext(privileged=True))
        else:
            container = client.V1Container(command=comand, args=comand_for_container , image = container_image , name = name_for_container, resources = resources,
                volume_mounts = volume_mounts,env=env_list)
        return container 

    #------------------------------------------------ Check functions -------------------------------------------------------#

    def check_namespace(self, namespace):
        """ 
            Checks the namespace for each class in the init section.
        """
        try:
            api_response = self.CoreV1Api.list_namespaced_persistent_volume_claim(namespace)
            self.logger.info('Checked namespace ' + namespace)
        except Exception as e:
            self.logger.error('Cloud not connect to ' + namespace + ' !\n'+ str(e))
            raise Exception('Could not connect to ' + namespace + ' ! '+ str(e))
        return namespace
