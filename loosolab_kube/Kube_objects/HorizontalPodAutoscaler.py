#!/usr/bin/env python3
import kubernetes
from kubernetes import client, config
from kubernetes.client.rest import ApiException
import sys
# self import

from .Deployment import Deployment
from loosolab_kube.Utils.utils import Utils

class HorizontalPodAutoscaler(Deployment):

    def __init__(self, namespace, level=3, config_file= False):
        """
            init Class creates an empty HPA object and checks the namespace
            The api instance gt loaded from the main Kube class.
        """
        super().__init__(namespace,  tool_name="HorizontalPodAutoscaler", level=level, config_file= config_file)
        self.body_of_HPA = client.V1HorizontalPodAutoscaler(api_version 	 = 'autoscaling/v1', kind = 'HorizontalPodAutoscaler')
    #--------------------------------------------------------------------------------------------------------#

    def build_HorizontalPodAutoscaler_obj(self, name_of_HPA, name_of_Deployment, target_cpu_utilization_percentage=60, max_replicas = 4, min_replicas = 1):
        scale_target_ref = client.V1CrossVersionObjectReference(api_version= 'app/v1 ',kind = 'Deployment', name = name_of_Deployment )

        self.body_of_HPA.spec =   client.V1HorizontalPodAutoscalerSpec(max_replicas = max_replicas,
                    min_replicas = min_replicas ,target_cpu_utilization_percentage = target_cpu_utilization_percentage,
                    scale_target_ref = scale_target_ref )
        
        self.body_of_HPA.metadata = client.V1ObjectMeta(namespace = self.namespace, name = name_of_HPA )

        return self.body_of_HPA

    #--------------------------------------------------------------------------------------------------------#

    def check_if_HPA_ex(self, name_of_HPA):
        """
            Checks if a HPA exist in a given namespaces.
        """
        try:
            api_response = self.AutoscalingV1Api.list_namespaced_horizontal_pod_autoscaler(self.namespace, field_selector='metadata.name=' + name_of_HPA)
        except ApiException as e:
            self.logger.error('Could not list HorizontalPodAutoscaler' + str(e))
            raise Exception('Could not list HorizontalPodAutoscaler' + str(e))
        if len(api_response.items) != 0:
            self.logger.info('HorizontalPodAutoscaler {} exists!'.format(name_of_HPA))
            return True
        else:
            self.logger.warning('HorizontalPodAutoscaler {} didn\'t exists!'.format(name_of_HPA))
            return False

    #--------------------------------------------------------------------------------------------------------#

    def create_HPA(self, body_of_HPA):
        """
            Sends a HPA to the Cluster if it dosen't exists.
        """   
        if self.check_if_HPA_ex(body_of_HPA.metadata.name):
            self.logger.error('HPA {} already exists!'.format(body_of_HPA.metadata.name))
        if self.check_if_Deployment_ex(body_of_HPA.spec.scale_target_ref.name):
            self.logger.warning('Deployment for HPA {} didnot exists!'.format(body_of_HPA.spec.scale_target_ref.name))
        try:
            self.AutoscalingV1Api.create_namespaced_horizontal_pod_autoscaler(self.namespace, self.body_of_HPA)
            self.logger.info('Created HPA: ' + body_of_HPA.metadata.name)
        except ApiException as e:
            self.logger.error('Cloud not create HPA: ' + str(e))
            raise Exception('Could not create HPA: ' + str(e))


    #--------------------------------------------------------------------------------------------------------#

    def delete_HPA(self, name_of_HPA):
        """
            Deletes a HPA which runs on the Cluster.
        """
        if not self.check_if_HPA_ex(name_of_HPA):
            self.logger.error('Cloud not delted HPA {} didn\'t exists!'.format(name_of_HPA))
        try:#create new HPA
            self.AutoscalingV1Api.delete_namespaced_horizontal_pod_autoscaler(name_of_HPA, self.namespace)
            self.logger.info('Deleted HPA: '+ name_of_HPA)
        except ApiException as e:
            self.logger.error('Deleted HPA ERROR: ' + str(e))
            raise Exception('Delete HPA ERROR: ' + str(e))

    #--------------------------------------------------------------------------------------------------------#
    
    def list_all_HPAs(self, full_information = False):
        try:
            HPAs_of_namespace = self.AutoscalingV1Api.list_namespaced_horizontal_pod_autoscaler(self.namespace, pretty=True)
            if full_information:
                self.logger.info('Returned description of all HPAs.')
                return HPAs_of_namespace
            else:
                self.logger.info('Returned list of all HPAs names.')
                return [i.metadata.name for i in HPAs_of_namespace.items]
        except ApiException as e:
            self.logger.error('Listing all HPAs failed: ' + str(e))
            raise Exception('Listing all HPAs failed: ' + str(e))