#!/usr/bin/env python3
import kubernetes
from kubernetes import client, config
from kubernetes.client.rest import ApiException

import base64
import sys
# self import

from .Kube import Kube

class PVC(Kube):

    def __init__(self, namespace, level=3, config_file= False):
        super().__init__(namespace,  tool_name="PVC", level=level, config_file= config_file)
        #Checks the namespace and creates an empty PVC body
        self.body_of_PVC = client.V1PersistentVolumeClaim(kind = 'PersistentVolumeClaim', api_version = 'v1')
        self.body_of_PVC.metadata = kubernetes.client.V1ObjectMeta(namespace = self.namespace)

    #--------------------------------------------------------------------------------------------------------#

    def add_pvc_data(self, name_of_PVC, size, storage_class = 'managed-nfs-storage', accessMode = ['ReadWriteMany']):
        """
            required:
                name_of_PVC
                size: number gives size in Gigabyte
            optional:
                storage_class = the storage class of the persisten Volume on which the PVC should bound
                accessMode = define how Pods can access the volume
        """
        self.body_of_PVC.metadata.name = name_of_PVC
        self.body_of_PVC.spec =  {'accessModes': accessMode,'storageClassName': storage_class,'resources':{"requests": { "storage": size+'Gi'}}}
        self.body_of_PVC.status = {'accessModes':accessMode,'capacity': {'storage': size+'Gi'}}
        return self.body_of_PVC

    #--------------------------------------------------------------------------------------------------------#

    def check_if_pvc_ex(self, name_of_PVC):
        #Checks if a PVC already exists on the cluster 
        try:
            api_response = self.CoreV1Api.list_namespaced_persistent_volume_claim(self.namespace, field_selector='metadata.name=' + name_of_PVC)
        except  ApiException as e :
            self.logger.error('Cloud not list PVC' + str(e))
            raise Exception('Could not list PVC' + str(e))

        if len(api_response.items) != 0:
            self.logger.info('PVC {} exists!'.format(name_of_PVC))
            return True
        else:
            self.logger.warning('PVC {} didn\'t exists!'.format(name_of_PVC))
            return False

    #--------------------------------------------------------------------------------------------------------#

    def create_pvc(self, body_of_PVC):
        #Creates a PVC on the Cluster 
        if self.check_if_pvc_ex(body_of_PVC.metadata.name):
            self.logger.error('PVC {} already exists!'.format(body_of_PVC.metadata.name))
        try:
            self.CoreV1Api.create_namespaced_persistent_volume_claim(self.namespace, body_of_PVC)
            self.logger.info('Created PVC: ' + body_of_PVC.metadata.name)
        except ApiException as e:
            self.logger.error('Cloud not create PVC: ' + str(e))
            raise Exception('Could not create PVC: ' + str(e))


    #--------------------------------------------------------------------------------------------------------#

    def get_size_of_PVC(self, name_of_PVC):
        #Returns the sziese of an PVC which runs on the Cluster 
        if not self.check_if_pvc_ex(name_of_PVC):
            self.logger.error('PVC {} didn\'t exists!'.format(name_of_PVC))
        try:
            api_response = self.CoreV1Api.list_namespaced_persistent_volume_claim(self.namespace, field_selector='metadata.name=' + name_of_PVC)
            return api_response.items[0].status.capacity['storage']
        except  ApiException as e :
            self.logger.error('Cloud not get PVC' + str(e))
            raise Exception('Could not get PVC' + str(e))


    #--------------------------------------------------------------------------------------------------------#

    def delete_PVC(self, name_of_PVC):
        #Deletes a PVC which runs on the Cluster
        if not self.check_if_pvc_ex(name_of_PVC):
            self.logger.error('Cloud not delted PVC {} didn\'t exists!'.format(name_of_PVC))
        try:#create new secret
            self.CoreV1Api.delete_namespaced_persistent_volume_claim(name_of_PVC, self.namespace)
            self.logger.info('Deleted PVC: '+ name_of_PVC)
        except ApiException as e:
            self.logger.error('Deleted PVC ERROR: ' + str(e))
            raise Exception('Deleted PVC ERROR: ' + str(e))

    #--------------------------------------------------------------------------------------------------------#
    
    def list_all_PVC(self, full_information = False):
        try:
            pvcs_of_namespace = self.CoreV1Api.list_namespaced_persistent_volume_claim(self.namespace, pretty=True)
            if full_information:
                self.logger.info('Returned description of all PVC.')
                return pvcs_of_namespace
            else:
                self.logger.info('Returned list of all PVC names.')
                return [i.metadata.name for i in pvcs_of_namespace.items]
        except ApiException as e:
            self.logger.error('Listing all PVC failed: ' + str(e))
            raise Exception('Listing all PVC failed: ' + str(e))