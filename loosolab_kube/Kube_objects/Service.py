#!/usr/bin/env python3
import kubernetes
from kubernetes import client, config
from kubernetes.client.rest import ApiException
# self import

from .Kube import Kube

class Service(Kube):

    def __init__(self, namespace, level=3, config_file= False):
        super().__init__(namespace,  tool_name="Service", level=level, config_file= config_file)
        self.body_of_Service = client.V1Service()  # V1Serice
        self.body_of_Service.metadata = client.V1ObjectMeta()
        self.body_of_Service.spec = client.V1ServiceSpec()

    #--------------------------------------------------------------------------------------------------------#
    
    def build_Service_obj(self, name_of_Service, selector_deployment, protocols=['TCP'], ports= [80], names=['main-app-port']):
        """
            Creates Servicesobject.
            required:
                name_of_Service
                selector_deployment: Contains a key value dict which defines on which
                Deployment the Service should bound.
            optional:
                port: default port 80 must be them same as the container port 
                protocol: default Protocol is TCP
        """
        try:
            #metadata
            self.body_of_Service.metadata.name = name_of_Service
            self.body_of_Service.metadata.namespace =  self.namespace
            #spec
            
            service_ports = []
            for port, protocol, name in zip(ports, protocols, names):
                service_ports.append(kubernetes.client.V1ServicePort(port=port,protocol = protocol, name=name))
            
            self.body_of_Service.spec.ports = service_ports # [kubernetes.client.V1ServicePort(port=port,protocol = protocol)]
            self.body_of_Service.spec.selector = selector_deployment
            #self.body_of_Service.spec.type = selector_deployment
            return self.body_of_Service
        except Exception as e:
            raise Exception('Could not build Service ' + str(e))
    #--------------------------------------------------------------------------------------------------------#

    def check_if_Service_ex(self, name_of_Service):
        #Checks if service exists by his name returns True False
        try:
            api_response = self.CoreV1Api.list_namespaced_service(self.namespace, field_selector='metadata.name=' + name_of_Service)
        except ApiException as e :
            self.logger.error('Could not list Services' + str(e))
            raise Exception('Could not list Services' + str(e))
        if len(api_response.items) != 0:
            self.logger.info('Service {} exists!'.format(name_of_Service))
            return True
        else:
            self.logger.warning('Service {} doesn\'t exists!'.format(name_of_Service))
            return False

    #------------------------------------------------ Api and resources functions -------------------------------------------#

    def create_Service(self, body_of_Service):
        #Creates a Service on the cluster
        if self.check_if_Service_ex(body_of_Service.metadata.name):
            self.logger.error('Service {} already exists!'.format(body_of_Service.metadata.name))
        try:#create new Service
            self.CoreV1Api.create_namespaced_service(self.namespace, body_of_Service)
            self.logger.info('Created Service: ' + body_of_Service.metadata.name)
        except ApiException as e:
            self.logger.error('Could not create Service: ' + str(e))
            raise Exception('Could not create Service: ' + str(e))

    #--------------------------------------------------------------------------------------------------------#

    def delete_Service(self, name_of_Service):
        #deletes a Service by name
        if not self.check_if_Service_ex(name_of_Service):
            self.logger.error('Could not delete {}, Service doesn\'t exist!'.format(name_of_Service))
        try:#create new secret
            self.CoreV1Api.delete_namespaced_service(name_of_Service, self.namespace)
            self.logger.info('Deleted Service: '+ name_of_Service)
        except ApiException as e:
            self.logger.error('Could not delete Service: ' + str(e))
            raise Exception('Could not delete Service: ' + str(e))
    
    #--------------------------------------------------------------------------------------------------------#
    
    def list_all_Service(self, full_information = False):
        try:
            services_of_namespace = self.CoreV1Api.list_namespaced_service(self.namespace, pretty=True)
            if full_information:
                self.logger.info('Returned description of all Services.')
                return services_of_namespace
            else:
                self.logger.info('Returned list of all Service names.')
                return [i.metadata.name for i in services_of_namespace.items]
        except ApiException as e:
            self.logger.error('Listing all Services failed: ' + str(e))
            raise Exception('Listing all Services failed: ' + str(e))
