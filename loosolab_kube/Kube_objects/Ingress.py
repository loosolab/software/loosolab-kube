#!/usr/bin/env python3
import kubernetes
from kubernetes import client, config
from kubernetes.client.rest import ApiException
import sys

# self import
from .Kube import Kube
from loosolab_kube.Utils.utils import Utils
from loosolab_kube.__init__ import __version__


class Ingress(Kube, Utils):

    def __init__(self, namespace, level=3, config_file= False):
        super().__init__(namespace,  tool_name="Ingress", level=level, config_file= config_file)
        self.body_of_ingress = client.V1IngressClass(api_version="networking.k8s.io/v1", kind= 'Ingress')
        
    #--------------------------------------------------------------------------------------------------------#
    
    def build_Ingress_obj(self, name_of_ingress, service_name, url ,
            secretName = 'secret-fordnsencryption', port= 80, path= '/', 
            annotations={'nginx.ingress.kubernetes.io/rewrite-target': '/'}, path_type= 'ImplementationSpecific'):
        """
            Creates Ingressobject.
            required:
                name_of_ingress
                service_name: Name of the Service on which the Ingress should bound.
                url: url under which the Ingress is reachable
            optional:
                port: default port 80 must be them same as the container port 
                path: Path to the url of the Service
                annotations: needed to configure the Ingresscontroller
        """
        try:
            #print(client.V1IngressClassSpec)
            self.body_of_ingress.metadata = client.V1ObjectMeta(annotations=annotations, name = name_of_ingress)
            backend = client.V1IngressBackend(service =client.V1IngressServiceBackend(name=service_name, port=client.V1ServiceBackendPort(number=port)))

            http_rule = client.V1HTTPIngressRuleValue([client.V1HTTPIngressPath(backend,path, path_type= path_type)])
            rules = [client.V1IngressRule(url, http_rule)]
            tls = [client.V1IngressTLS([url], secretName)]

            self.body_of_ingress.spec = client.V1IngressSpec(rules = rules, tls= tls)
            return self.body_of_ingress
        except Exception as e:
            raise Exception('Could not build Ingress ' + str(e))

    #--------------------------------------------------------------------------------------------------------#

    def check_if_Ingress_ex(self, name_of_ingress):
        #Checks if a Ingress exists on the Cluster 
        try:
            api_response = self.NetworkingV1Api.list_namespaced_ingress(self.namespace, field_selector='metadata.name=' + name_of_ingress)
        except ApiException as e :
            self.logger.error('Cloud not list Ingress\'s' + str(e))
            raise Exception('Could not list Ingresses' + str(e))
        if len(api_response.items) != 0:
            self.logger.info('Ingress {} exists!'.format(name_of_ingress))
            return True
        else:
            self.logger.warning('Ingress {} doesn\'t exists!'.format(name_of_ingress))
            return False

    #------------------------------------------------ Api and resources functions -------------------------------------------#

    def create_Ingress(self, body_of_ingress):
        #Sends an Ingress object to the Cluster.
        if self.check_if_Ingress_ex(body_of_ingress.metadata.name):
            self.logger.error('Ingress {} already exists!'.format(body_of_ingress.metadata.name))
        try:#create new secret
            self.NetworkingV1Api.create_namespaced_ingress(self.namespace, body_of_ingress)
            self.logger.info('Created Ingress: ' + body_of_ingress.metadata.name)
        except ApiException as e:
            self.logger.error('Could not create Ingress: ' + str(e))
            raise Exception('Could not create Ingress: ' + str(e))
    #--------------------------------------------------------------------------------------------------------#

    def delete_Ingress(self, name_of_ingress):
        #Deletes an Ingress object on the Cluster.
        if not self.check_if_Ingress_ex(name_of_ingress):
            self.logger.error('Could not delete Ingress {}, it doesn\'t exists!'.format(name_of_ingress))
        try:#create new secret
            self.NetworkingV1Api.delete_namespaced_ingress(name_of_ingress, self.namespace)
            self.logger.info('Deleted Ingress: '+ name_of_ingress)
        except ApiException as e:
            self.logger.error('Could not delete Ingress: ' + str(e))
            raise Exception('Could not delete Ingress: ' + str(e))

    #--------------------------------------------------------------------------------------------------------#
    
    def list_all_Ingress(self, full_information = False):
        try:
            ingress_of_namespace = self.NetworkingV1Api.list_namespaced_ingress(self.namespace, pretty=True)
            if full_information:
                self.logger.info('Returned description of all Ingress.')
                return ingress_of_namespace
            else:
                self.logger.info('Returned list of all Ingress names.')
                return [i.metadata.name for i in ingress_of_namespace.items]
        except ApiException as e:
            self.logger.error('Listing all Ingress failed: ' + str(e))
            raise Exception('Listing all Ingresses failed: ' + str(e))
