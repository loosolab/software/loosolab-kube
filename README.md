# Kubernetes_Python_loosolab_Package

This Package contains functions to work with the most recent Kubernetesobjects.

**Installation**
------------
Clone the git Repository, create a python conda environment and run the setup.py.

```bash
$ git clone git@gitlab.gwdg.de:loosolab/software/loosolab-kube.git
$ cd loosolab-kube/
$ conda create -n kubernetes  python=3.6
$ conda activate kubernetes
$ python setup.py install
```
Or install it with pip.
```bash
$ pip install git+https://gitlab.gwdg.de/loosolab/software/loosolab-kube.git
```

**Description of Functions**
------------
Each Kubernetesobject is represented by an own pythonclass. Currently the following Objects are supported by the package: **Deployment**, **Ingress**, **Job**, **Pod**, **PVC**, **Secret** and **Service**. Each object have got four main functions: create_object(creates the object on the cluster), build_object(helps to create the description for an object), delete_object and 
check_for_object(Checks if the object runs ón the cluster). If you create a instances of a object a empty body of the object is 
created this body can then be filled with the build_objs functions of each object or by hand. For example:

```python
import loosolab_kube
service = loosolab_kube.Kube_objects.Service(<namespace>)
print(service.body_of_Service)
body_of_Service.metadata.name = 'myservice'
```
This is also very helpful if you want to add specifications to your object which aren't supported by the build_obj function of the object.

**Ingress**

For each object you first of all have to specify the namespace on which you are working. After this you can build the 
description of the object as shown bellow. The returned description can then be send to the cluster to create the object.

```python
import loosolab_kube
ingress = loosolab_kube.Kube_objects.Ingress(<namespace>)
body_of_ingress = ingress.build_Ingress_obj(name_of_ingress, service_name, url,secretName = 'letsencrypt-prod-private-key',
                 port= 80, path= '/', annotations={'nginx.ingress.kubernetes.io/rewrite-target': '/'})
        """
            Returns Ingressobject.
            required:
                name_of_ingress
                service_name: Name of the Service on which the Ingress should bound.
                url: url under which the Ingress is reachable
            optional:
                port: default port 80 must be them same as the container port 
                path: Path to the url of the Service
                annotations: needed to configure the Ingresscontroller
                secretName: defines the secret name for TSL secure
        """
ingress.create_Ingress(body_of_ingress)
#Sends an Ingress object to the Cluster. 

boolean = ingress.check_if_Ingress_ex(name_of_ingress)
#Check if a Ingress exist on the cluster by his name returns True or False

ingress.delete_Ingress(name_of_ingress)
#Deletes an Ingress object on the Cluster
```

**PVC**

This Functions creates Persistent Volume Claims on the cluster. Persistent Volume Claims can be used to store data on
the cluster and exchange it between Jobs and Deployments.
```python
import loosolab_kube
pvc = loosolab_kube.Kube_objects.PVC(<namespace>)
body_of_PVC = pvc.add_pvc_data(name_of_PVC, size, storage_class = 'managed-nfs-storage', accessMode = ['ReadWriteMany'])
"""
    required:
        name_of_PVC
        size: number gives size in Gigabyte
    optional:
        storage_class = the storage class of the persistent Volume on which the PVC should bound
        accessMode = define how Pods can access the volume
"""

boolean = pvc.check_if_pvc_ex( name_of_PVC):
#Checks if a PVC already exists on the cluster

pvc.create_pvc(body_of_PVC):
#Creates a PVC on the Cluster 

pvc.get_size_of_PVC(name_of_PVC):
#Returns the sizes of an PVC which runs on the Cluster

pvc.delete_PVC(name_of_PVC):
#Deletes a PVC on the Cluster.
```

**Secret**

In a secret on Kubernetes you can save sensible data like passwords for example the credentials for an S3-storage.

```python
import loosolab_kube
secret = loosolab_kube.Kube_objects.Secret(<namespace>)
body_of_secret = secret.build_s3_secret(endpoint, s3key, s3secret, name_of_secret)
"""
    Creates a S3-Secret the values are accessible by the containers on the 
    cluster over the keys s3endpoint, s3key and s3secret.
"""

body_of_secret = secret.build_secret(data_dict, name_of_secret)
"""
    data_dict = {key1:secret_value1, key2:secret_value2}
    Creates a Secret which  contains the key:values from a dict.
    Key values get automated convert to base64 format.
"""
secret.create_secret(body_of_secret)
#Creates a secret on the cluster

boolean = secret.check_if_secret_ex(name_of_secret)
#Checks if a secret exists on the cluster 

secret.delete_secret(name_of_secret)
#delete a secret on the Cluster

secret.get_secret_data(name_of_secret)
#returns the data of a secret on the cluster as a dict.
```

**Service**

A Service on Kubernetes creates the connection between a Deployment and a Ingress to exposes a DEployment.  
```python
import loosolab_kube
service = loosolab_kube.Kube_objects.Service(<namespace>)
body_of_Service = service.build_Service_obj(name_of_Service, selector_deployment, protocol='TCP', port= 80)
"""
    Creates Serviceobject.
    required:
        name_of_Service
        selector_deployment: Contains a key value dict which defines on which
        Deployment the Service should bound.
    optional:
        port: default port 80 must be them same as the container port 
        protocol: default Protocol is TCP
"""
boolean = service.check_if_Service_ex(name_of_Service)
#Checks if service exists by his name returns True False

service.create_Service(body_of_Service)
#Creates a Service on the cluster

service.delete_Service(name_of_Service)
#deletes a Service by name
```

# Deployment Objects

The Objects Deployment, Pod and Job need several equal objects such as container descriptions,
volumemounts and secret env-variables. These objects can be created with the Kube main function 
which is owned by these objects. So if import Deployment, Pod or Job these functions are also available.

**Volumes**

The Package has two Volume function the first is need to mount the Volume inside a Deployment, Job or Pod. 
The second one to use the mounted volume inside a container. 

```python
import loosolab_kube
Deployment = loosolab_kube.Kube_objects.Deployment(<namespace>)
volumes = Deployment.volumes(name, name_of_PVC=None )
"""
    Creates an emptydir volume of name_of_Pvc isn't set if a PVC volume is set it's creates a
    PVC description.

    name: Name of Volume 

    name_of_PVC: name des PVCs

    return:

    If PVC:
        PVC instances which can be added to Pod, Job or Deployment.
    else:
        empty dir instance can be added to Pod, Job or Deployment.
"""
#For the Container
volume_mounts = Deployment.volume_mounts(name, mount_path)
"""
    Returns mountpath description for Container to mount an PVC or emptydir.
"""
```

**Env Secret**

With this function you can ad the description of an Secret which should be mounted inside a container.
```python
import loosolab_kube
Deployment = loosolab_kube.Kube_objects.Deployment(<namespace>)
env_list = []
env_list.append(Deployment.env_variables(name_of_value, key_of_secret, name_of_secret))
"""
    name_of_value: define the name under which the value is accessible on the comandline later on.
    name_of_secret: Name of the Secret on the cluster.
"""
```

**create_container_object**

This function creates you a container description. The returned object can then be add to 
the Deployment, Job and Pod build function. 
```python
import loosolab_kube
Deployment = loosolab_kube.Kube_objects.Deployment(<namespace>)

Deployment.container( name_for_container, container_image, comand_for_container=None, comand=None,
     resources={"requests": {'memory': "128Mi", 'cpu': "1000m"},"limits": {'memory': "128Mi", 'cpu': "1000m"}}
        , container_port = 80, volume_mounts=[], env_list=[])
        """
            Creates a Container instance
            required:
            name_for_Container
            container_image
            optional:
                comand_for_container = Comandline descriptions for example ['-c', 'ls']
                comand =  which comand should use to interpret the container comand for example use ['bash/sh']
                resources = resources of container in a dict with limits and requests
                container_port = default 80 need for expose of traffic
                volume_mounts = volumes which should be mounted iin the container. To create use the volume_mounts function.
                env_list = List of env variables for the container. To create use the variables function.
            return:
                Container description
        """
```


**Deployment**

To create a Deployment you need a container object this can be created with the container function explained above.
```python
import loosolab_kube
deployment = loosolab_kube.Kube_objects.Deployment(<namespace>)
body_of_Deployment = deployment.build_Deployment_obj(name_of_Deployment, container, match_labels_dict = {'run':'test'}, init_container=None,
    volumes=[],replicas = 1)
"""
    required:
        name_of_Deployment
        container = use the the container function to create an container object, must be given as list.
    optional:
        match_labels_dict = labels to bound on a Service object.
        init_container = add a InitContainer to the Deployment can also be created with the container function,
                         must be given as list.
        volumes = add volumes to the Deployment use the volumes function to create Volumeobjects.
        replicas = The number of replicas which is created on the Cluster.
"""
boolean = deployment.check_if_Deployment_ex(name_of_Deployment)
#Checks if a Deployment exist in a given namespaces.

deployment.create_Deployment(body_of_Deployment)
#Sends a Deployment to the Cluster if it doesn't exists.

deployment.delete_Deployment(name_of_Deployment)
#Deletes a Deployment which runs on the Cluster.
```

**Job**

A Job needs also a container object. 
```python
import loosolab_kube
job = loosolab_kube.Kube_objects.Job(<namespace>)
job.build_Job_obj(name_of_Job, container, init_container=None,volumes=[], backoff_limit=3,ttl_seconds_after_finished=600)
"""
    Creates an Job object.
        required:
            name_of_Job
            container = Container objects, must be given as list. Create them with the container Function must be given as list
        optional:
            init_container = Create an Initcontainer for the Job, must be given as list. Can also be created with the container function.
            volumes = Volumes which should be mounted use the volumes Function
            backoff_limit: The number of restarts a Job gets if it crashes.
            ttl_seconds_after_finished: The seconds the Job get automated be deleted on the cluster after it has finished.

"""

boolean = job.check_if_Job_ex(name_of_Job)
#Checks if Job exists returns a True or False

job.create_Job(body_of_Job)
#Creates a Job on the Cluster

job.delete_Job(name_of_Job)
#Creates a Job on the Cluster
```
**Pod**

A Pod is the smallest deployable unit of Kubernetes.
```python
import loosolab_kube
pod = loosolab_kube.Kube_objects.Pod(<namespace>)
pod.build_Pod_obj(name_of_Pod, container, init_container=None)
"""
    creates a Podobject.
    required: 
        name_of_Pod
        container = container description can be created with Container function, must be given as list.
    optional:
        init_Container = Creates an init_Container for the Pod must be given as list.
"""
boolean = pod.check_if_Pod_ex(name_of_Pod):
#Checks if a Pod exists on the Cluster. returns True or False.

pod.create_Pod(body_of_Pod ):
#Sends a Give Pod object to the Cluster if it isn't Present.

pod.delete_Pod(name_of_Pod):
#Deletes a Pod on the Cluster
```

