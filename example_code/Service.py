import loosolab_kube
service = loosolab_kube.Kube_objects.Service('mampok')

body_of_Service = service.build_Service_obj('myservice',  {'run':'test'}, protocol='TCP', port= 80)
"""
    Creates Serviceobject.
    required:
        name_of_Service
        selector_deployment: Contains a key value dict which defines on which
        Deployment the Service should bound.
    optional:
        port: default port 80 must be them same as the container port 
        protocol: default Protocol is TCP
"""
boolean = service.check_if_Service_ex('myservice')
#Checks if service exists by his name returns True False

service.create_Service(body_of_Service)
#Creates a Service on the cluster

#service.delete_Service('myservice')
#deletes a Service by name