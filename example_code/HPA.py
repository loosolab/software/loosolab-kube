from loosolab_kube.Kube_objects.HorizontalPodAutoscaler import HorizontalPodAutoscaler

hpa = HorizontalPodAutoscaler('mampok')


hpa_body = hpa.build_HorizontalPodAutoscaler_obj('myhpa', 'mydeployment', target_cpu_utilization_percentage=60,
                                                         max_replicas = 4, min_replicas = 1)
"""
target_cpu_utilization_percentage = percentage of cpu ussage when upscale

"""

boolean = hpa.check_if_HPA_ex('myhpa')

print(boolean)

hpa.create_HPA(hpa_body)

boolean = hpa.check_if_HPA_ex('myhpa')

print(boolean)

print(hpa.list_all_HPAs())

hpa.delete_HPA('myhpa')