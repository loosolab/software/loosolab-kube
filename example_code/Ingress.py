#!/usr/bin/env python3
import kubernetes
from kubernetes import client, config
from kubernetes.client.rest import ApiException
import sys
def main():
	# Configs can be set in Configuration class directly or using helper
	# utility. If no argument provided, the config will be loaded from
	# default location.
	config.load_kube_config()
	apps_v1 = client.AppsV1Api()
	return apps_v1
app = main()
#print(app.list_namespaced_deployment('mampok', pretty=True))

import loosolab_kube

deployment = loosolab_kube.Kube_objects.Deployment('mampok')
print(deployment.list_all_Deployments())
"""
print(loosolab_kube.__file__)
ingress = loosolab_kube.Kube_objects.Ingress('mampok')
body_of_ingress = ingress.build_Ingress_obj('myingress', 'myservice', 'mpibn.mampok.ingress.rancher.computational.bio',path='/sdfsdf')
print(body_of_ingress)
boolean = ingress.check_if_Ingress_ex('myingress')
print(boolean)
if boolean:
	ingress.delete_Ingress('myingress')
ingress.create_Ingress(body_of_ingress)

body_of_ingress = ingress.build_Ingress_obj('myingress', 'myservice','mpibn-mampok.134.176.27.161.xip.io',secretName = 'letsencrypt-prod-private-key',
					 port= 80, path= '/test/', annotations={'nginx.ingress.kubernetes.io/rewrite-target': '/',
					'nginx.ingress.kubernetes.io/ingress.class': 'nginx','cert-manager.io/issuer': 'letsencrypt-prod', 
					'nginx.ingress.kubernetes.io/configuration-snippet':'|2-rewrite ^(/betsholtz-lung-vasculature/cellxgene)$ $1/ redirect;'})
print(body_of_ingress)

	Returns Ingressobject.
	required:
		name_of_ingress
		service_name: Name of the Service on which the Ingress should bound.
		url: url under which the Ingress is reachable
	optional:
		port: default port 80 must be them same as the container port 
		path: Path to the url of the Service
		annotations: needed to configure the Ingresscontroller

ingress.create_Ingress(body_of_ingress)
#Sends an Ingress object to the Cluster. 

boolean = ingress.check_if_Ingress_ex('myingress')
#Check if a Ingress exist on the cluster by his name returns True or False

ingress.delete_Ingress('myingress')
#Deletes an Ingress object on the Cluster"""