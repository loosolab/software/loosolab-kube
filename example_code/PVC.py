import loosolab_kube

pvc = loosolab_kube.Kube_objects.PVC('macsek')

body_of_PVC = pvc.add_pvc_data('mypvc', '2', storage_class = 'managed-nfs-storage', accessMode = ['ReadWriteMany'])
"""
    required:
        name_of_PVC
        size: number gives size in Gigabyte
    optional:
        storage_class = the storage class of the persistent Volume on which the PVC should bound
        accessMode = define how Pods can access the volume
"""
print(body_of_PVC)

#Checks if 'mypvc' PVC already exists on the cluster.
boolean = pvc.check_if_pvc_ex('mypvc')
print(boolean)

pvc.create_pvc(body_of_PVC)
#Creates a PVC on the Cluster 

size = pvc.get_size_of_PVC('mypvc')
print(size)
#Returns the sizes of an PVC which runs on the Cluster

pvc.delete_PVC('mypvc')
#Deletes a PVC on the Cluster."""