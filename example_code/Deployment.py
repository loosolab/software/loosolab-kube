import loosolab_kube

deployment = loosolab_kube.Kube_objects.Deployment('mampok')

container_list = []

container_list.append(deployment.container('mycontainer', 'nginx', comand_for_container=None, comand=None,
     resources={"requests": {'memory': "128Mi", 'cpu': "1000m"},"limits": {'memory': "128Mi", 'cpu': "1000m"}}
        , container_port = 80, volume_mounts=[], env_list=[]))


body_of_Deployment = deployment.build_Deployment_obj('mydeployment', container_list, match_labels_dict = {'run':'test'}, init_container=None,
    volumes=[],replicas = 1)
print(body_of_Deployment)
"""
    required:
        name_of_Deployment
        container = use the the container function to create an container object, must be given as list.
    optional:
        match_labels_dict = labels to bound on a Service object.
        init_container = add a InitContainer to the Deployment can also be created with the container function,
                         must be given as list.
        volumes = add volumes to the Deployment use the volumes function to create Volumeobjects.
        replicas = The number of replicas which is created on the Cluster.
"""
#boolean = deployment.check_if_Deployment_ex('mydeployment')
#Checks if a Deployment exist in a given namespaces.
#print(boolean)


#deployment.create_Deployment(body_of_Deployment)
#Sends a Deployment to the Cluster if it doesn't exists.

#deployment.delete_Deployment('mydeployment')
#Deletes a Deployment which runs on the Cluster."""