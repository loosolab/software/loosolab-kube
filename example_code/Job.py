import loosolab_kube

job = loosolab_kube.Kube_objects.Job('mampok')

container_list = []

container_list.append(job.container('mycontainer', 'debian', comand_for_container=['-c', 'ls'], comand=['bin/bash'],
     resources={"requests": {'memory': "128Mi", 'cpu': "1000m"},"limits": {'memory': "128Mi", 'cpu': "1000m"}}
        , container_port = 80, volume_mounts=[], env_list=[]))

body_of_Job = job.build_Job_obj('myjob', container_list, init_container=None,volumes=[], backoff_limit=3,ttl_seconds_after_finished=600)
"""
    Creates an Job object.
        required:
            name_of_Job
            container = Container objects, must be given as list. Create them with the container Function must be given as list
        optional:
            init_container = Create an Initcontainer for the Job, must be given as list. Can also be created with the container function.
            volumes = Volumes which should be mounted use the volumes Function
            backoff_limit: The number of restarts a Job gets if it crashes.
            ttl_seconds_after_finished: The seconds the Job get automated be deleted on the cluster after it has finished.

"""

boolean = job.check_if_Job_ex('myjob')
#Checks if Job exists returns a True or False

job.create_Job(body_of_Job)
#Creates a Job on the Cluster

job.delete_Job('myjob')
#Creates a Job on the Cluster
